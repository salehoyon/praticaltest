<?php
require_once('Task.php');

$categoWiseProducts = (new Task())->showAllCategories();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Task 1</title>
</head>
<body>
<div id="item-count-div">
    <table border="1">
        <thead>
        <tr>
            <th>Category Name</th>
            <th>Total Items</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($categoWiseProducts as $categoryWiseProduct): ?>
            <tr>
                <td><?= $categoryWiseProduct['category'] ?></td>
                <td><?= $categoryWiseProduct['totalItems'] ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
</body>
</html>