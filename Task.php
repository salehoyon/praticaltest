<?php

require_once('Database.php');

class Task
{
    private $db;
    private $conn;

    public function __construct()
    {
        $this->db   = Database::getInstance();
        $this->conn = $this->db->getConnection();
    }

    public function showAllCategories()
    {
        $selectQuery = "SELECT `category`.`Name` as category, COUNT(1) as totalItems 
FROM `item_category_relations`
join `category` ON `category`.`Id` = `item_category_relations`.`categoryId`
GROUP BY `category`.`Name`
order by `totalItems` desc";

        try {
            $stmt = $this->conn->query($selectQuery);
        } catch (PDOException $e) {
            $this->db->errors($e);
        }

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getCategories()
    {
        $selectQuery = "SELECT category.Id as id, category.Name as name, COUNT(1) as total
FROM `category`
join catetory_relations ON category.Id = catetory_relations.ParentcategoryId
group by catetory_relations.ParentcategoryId";

        try {
            $stmt = $this->conn->query($selectQuery);
        } catch (PDOException $e) {
            $this->db->errors($e);
        }

        $parents = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $categories = [];

        foreach ($parents as $parent) {
            if ($this->isChild($parent['id'])) {
                continue;
            }

            $children = $this->findChildren($parent['id']);

            foreach ($children as $i => $child) {
                $children[$i]['children'] = [];
                $children[$i]['children'] = $this->findChildren($child['id']);
            }

            $categories[] = [
                'id'         => $parent['id'],
                'name'       => $parent['name'],
                'totalChild' => $parent['total'],
                'children'   => $children
            ];
        }

        return $categories;
    }

    private function findChildren($id)
    {
        $selectQuery = "SELECT category.Id as id, category.Name as name 
FROM `category`
join catetory_relations ON category.Id = catetory_relations.categoryId
WHERE catetory_relations.ParentcategoryId = ?";

        try {
            $stmt = $this->conn->prepare($selectQuery);
            $stmt->execute([$id]);
        } catch (PDOException $e) {
            $this->db->errors($e);
        }

        $children = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $children;
    }

    private function isChild($id)
    {
        $selectQuery = "SELECT COUNT(1) as `total` FROM `catetory_relations` WHERE catetory_relations.categoryId = ?";

        try {
            $stmt = $this->conn->prepare($selectQuery);
            $stmt->execute([$id]);
        } catch (PDOException $e) {
            $this->db->errors($e);
        }

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row['total'] ;
    }
}