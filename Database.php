<?php

class Database
{
    private static $instance = null;
    private $conn;
    private $SERVER = 'localhost';
    private $user   = 'root';
    private $pass   = '';
    private $db     = 'ecommerce';

    private function __construct()
    {
        try {
            $this->conn = new PDO('mysql:host='.$this->SERVER.';dbname='.$this->db.';charset=utf8', $this->user, $this->pass);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (PDOException $e) {
            $this->errors($e);
        }
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Database();
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->conn;
    }

    public function errors($e)
    {
        echo 'Error Message: '.$e->getMessage(). '<br/>';
        echo 'Error Code: '.$e->getCode().'<br/>';
        echo 'Error File Path: '.$e->getFile().'<br/>';
        echo 'Line Number: '.$e->getLine().'<br/>';
        echo 'Trace: ';
        echo '<pre>';
        var_dump($e->getTrace());
        echo '</pre>';
    }
}